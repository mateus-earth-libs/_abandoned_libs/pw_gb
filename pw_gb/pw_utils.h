#ifndef __PW_UTILS_H__
#define __PW_UTILS_H__

// pw
#include "pw_defs.h"


//----------------------------------------------------------------------------//
// Misc                                                                       //
//----------------------------------------------------------------------------//
void Delay(U8 count);


//----------------------------------------------------------------------------//
// Input                                                                      //
//----------------------------------------------------------------------------//
extern U8 Input_Curr;
extern U8 Input_Prev;

#define Input_Reset() { \
    Input_Curr = 0;     \
    Input_Prev = 0;     \
}

#define Input_BeginFrame() { Input_Curr = joypad();   }
#define Input_EndFrame()   { Input_Prev = Input_Curr; }

#define JOY_PRESS(b) ((Input_Curr & b))
#define JOY_CLICK(b) ((Input_Curr & b) != (Input_Prev & b) && (Input_Curr & b) != 0)


//----------------------------------------------------------------------------//
// Random                                                                     //
//----------------------------------------------------------------------------//
void Random_Init(U8 seed);
U8   Random_U8(U8 _min, U8 _max);

#define Random() rand()


//----------------------------------------------------------------------------//
// GFX                                                                        //
//----------------------------------------------------------------------------//
I8 gprintxy(U8 x, U8 y, char *fmt, ...) NONBANKED;

#endif // __PW_UTILS_H__
