// Header
#include "pw_utils.h"
// std
#include <stdarg.h>


//----------------------------------------------------------------------------//
// Misc                                                                       //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Delay(U8 count)
{
    while(count-- > 0) {
        wait_vbl_done();
    }
}


//----------------------------------------------------------------------------//
// Input                                                                      //
//----------------------------------------------------------------------------//
// Public
U8 Input_Curr;
U8 Input_Prev;



//----------------------------------------------------------------------------//
// Random                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Random_Init(U8 seed)
{
    // @TODO(stdmatt): Make the random for release
#if GAME_RELEASE
    initrand(seed);
#else
    initrand(seed);
#endif // GAME_RELEASE
}


//------------------------------------------------------------------------------
U8
Random_U8(U8 _min, U8 _max)
{
    return _min + (((U8)rand()) % (_max - _min));
}



//------------------------------------------------------------------------------
I8 gprintxy(U8 x, U8 y, char *fmt, ...) NONBANKED
{

  va_list ap;
  INT8 nb = 0;

  gotogxy(x, y);
  va_start(ap, fmt);
  for(; *fmt; fmt++)
    if(*fmt == '%') {
      switch(*++fmt) {
      case 'c': {
	/* char */
	  char c = va_arg(ap, char);
	  wrtchr(c);
	  break;
      }
      case 'd': {
	/* decimal int */
	  INT8 b = va_arg(ap, INT8);
	  gprintn(b, 10, SIGNED);
	  break;
      }
      case 'u': {
	  /* unsigned int */
	  INT8 b = (INT8)va_arg(ap, int);
	  gprintn(b, 10, UNSIGNED);
	  break;
      }
      case 'o': {
	  /* octal int */
	  INT8 b = va_arg(ap, INT8);
	  gprintn(b, 8, UNSIGNED);
	  break;
      }
      case 'x': {
	  /* hexadecimal int */
	  INT8 b = va_arg(ap, INT8);
	  gprintn(b, 16, UNSIGNED);
	  break;
      }
      case 's': {
	  /* string */
	  char *s = va_arg(ap, char *);
	  gprint(s);
	  break;
      }
#if 0
      case 'l':
	/* long */
	switch(*++fmt) {
	case 'd':
	  /* decimal long */
	  gprintln(va_arg(ap, INT16), 10, SIGNED);
	  break;
	case 'u':
	  /* unsigned long */
	  gprintln(va_arg(ap, INT16), 10, UNSIGNED);
	  break;
	case 'o':
	  /* octal long */
	  gprintln(va_arg(ap, INT16), 8, UNSIGNED);
	  break;
	case 'x':
	  /* hexadecimal long */
	  gprintln(va_arg(ap, INT16), 16, UNSIGNED);
	  break;
	}
	break;
#endif
      case '%':
	/* % */
	wrtchr(*fmt);
	break;
      default:
	return -1;
      }
      nb++;
    } else
      wrtchr(*fmt);
  va_end(ap);

  return nb;
}
