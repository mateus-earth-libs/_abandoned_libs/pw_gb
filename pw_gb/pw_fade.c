// Header
#include "pw_fade.h"


//----------------------------------------------------------------------------//
// MACROS                                                                     //
//----------------------------------------------------------------------------//
#define FADE_0 0xFFU
#define FADE_1 0xFEU
#define FADE_2 0xF9U
#define FADE_3 0xE4U

#define SET_FADE_0() do { BGP_REG = 0xFFU; } while(0);
#define SET_FADE_1() do { BGP_REG = 0xFEU; } while(0);
#define SET_FADE_2() do { BGP_REG = 0xF9U; } while(0);
#define SET_FADE_3() do { BGP_REG = 0xE4U; } while(0);


//----------------------------------------------------------------------------//
// Globals                                                                    //
//----------------------------------------------------------------------------//
// Public
BOOL Fade_IsCompleted = FALSE;
U8   Fade_State       = FADE_OUT;
// Private
static U16 _fade_frames_total = 0;
static U16 _fade_frames       = 0;


//----------------------------------------------------------------------------//
// Private Functions                                                          //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
BOOL
_Fade_In()
{
    U16 i;

    //
    // We already completed all the frames of the fade animation.
    if(_fade_frames > _fade_frames_total) {
        Fade_IsCompleted = TRUE;
        return TRUE;
    }

    //
    // Fade animation has 4 stages, so we need to divide the total frames
    // to actually know in which step of the animation we are in.
    i = (_fade_frames_total / 4);
    ++_fade_frames;

    //
    // Just set the background pallet register
    if     (_fade_frames < i    ) { SET_FADE_0(); }
    else if(_fade_frames < i * 2) { SET_FADE_1(); }
    else if(_fade_frames < i * 3) { SET_FADE_2(); }
    else if(_fade_frames < i * 4) { SET_FADE_3(); }

    return FALSE;
}

//------------------------------------------------------------------------------
BOOL
_Fade_Out()
{
    U16 i;

    //
    // We already completed all the frames of the fade animation.
    if(_fade_frames > _fade_frames_total) {
        Fade_IsCompleted = TRUE;
        return TRUE;
    }

    //
    // Fade animation has 4 stages, so we need to divide the total frames
    // to actually know in which step of the animation we are in.
    i = (_fade_frames_total / 4);
    ++_fade_frames;

    // Just set the background pallette register
    if     (_fade_frames < i    ) { SET_FADE_3(); }
    else if(_fade_frames < i * 2) { SET_FADE_2(); }
    else if(_fade_frames < i * 3) { SET_FADE_1(); }
    else if(_fade_frames < i * 4) { SET_FADE_0(); }

    return FALSE;
}


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Fade_Reset(U16 frames, U8 state)
{
    _fade_frames       = 0;
    _fade_frames_total = frames;

    Fade_IsCompleted = FALSE;
    Fade_State       = state;
}

//------------------------------------------------------------------------------
BOOL
Fade_Update()
{
    return (Fade_State == FADE_OUT)
        ? _Fade_Out()
        : _Fade_In ();
}
