#ifndef __PW_DEFS_H__
#define __PW_DEFS_H__

#include <gb/gb.h>
#include <gb/drawing.h>
#include <types.h>
#include <stdio.h>
#include <rand.h>

//----------------------------------------------------------------------------//
// Types                                                                      //
//----------------------------------------------------------------------------//
#define BOOL UINT8
#define U8   UINT8
#define I8   INT8
#define I16  INT16
#define U16  UINT16

//
//
#define TRUE  1
#define FALSE 0


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define TILE_SIZE     8
#define TILE_SIZE_x2 16
#define TILE_SIZE_x3 24
#define TILE_SIZE_x4 32

#define SCREEN_WIDTH  SCREENWIDTH
#define SCREEN_HEIGHT SCREENHEIGHT

#define FIRST_PIXEL_X   8
#define FIRST_PIXEL_Y  16
#define LAST_PIXEL_X  168
#define LAST_PIXEL_Y  160

#endif // __PW_DEFS_H__
